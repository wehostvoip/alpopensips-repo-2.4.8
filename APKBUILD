# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=opensips
pkgver=2.4.8
pkgrel=0
pkgdesc="Flexible and customizable sip routing engine"
url="http://www.opensips.org/"
arch="all"
license="GPL"
depends=
makedepends="gcc6 libxml2-dev linux-headers mysql-dev curl-dev util-linux-dev ncurses-dev pcre-dev openssl-dev
perl xmlrpc-c-dev perl-term-readkey bison flex expat-dev coreutils lksctp-tools-dev perl-dev postgresql-dev python2-dev
unixodbc-dev confuse-dev sqlite-dev lua-dev openldap openldap-dev openssl-dev net-snmp-dev libuuid libev-dev
jansson-dev json-c-dev libevent-dev libmemcached-dev rabbitmq-c-dev hiredis-dev libmaxminddb-dev freeradius-client-dev libuv-dev thrift
thrift-dev mongo-c-driver-dev libcouchbase-dev"

install="$pkgname.pre-install $pkgname.post-install"
subpackages="$pkgname-doc $pkgname-b2bua $pkgname-berkeley $pkgname-compression $pkgname-dbhttp $pkgname-dialplan $pkgname-emergency
$pkgname-geoip $pkgname-http $pkgname-identity $pkgname-jabber $pkgname-ldap $pkgname-lua $pkgname-memcached $pkgname-mysql
$pkgname-perl $pkgname-postgres $pkgname-presence $pkgname-rabbit $pkgname-radius $pkgname-redis
$pkgname-regex $pkgname-restclient $pkgname-sctp $pkgname-snmpstats $pkgname-sqlite $pkgname-tls $pkgname-tlsmgm $pkgname-unixodbc $pkgname-wss $pkgname-xmlrpc $pkgname-xmpp"

source="http://download.opensips.org/$pkgver/$pkgname-$pkgver.tar.gz $pkgname.initd 10-resolve.patch 20-socket.patch 30-mathops.patch 40-tls_mgm.patch 50-ldsession.patch"

build() {
	cd "$srcdir/$pkgname-$pkgver"
	sed -i -e 's:^cfg-target.*:cfg-target = $(cfg-dir):' \
		-e 's:^cfg-prefix.*:cfg-prefix = $(basedir):' Makefile.defs

	cd scripts
	sed -i -e 's:/var/run/opensips.pid:/var/run/opensips/opensips.pid:g' \
		opensipsctl.base opensipsctlrc osipsconsole osipsconsolerc
	cd ..

	make prefix=/usr || return 1
}

package() {
        export RADIUSCLIENT=FREERADIUS
	cd "$srcdir/$pkgname-$pkgver"
	make prefix=/usr include_modules="aaa_radius b2b_logic cachedb_memcached cachedb_redis cachedb_cassandra cachedb_couchbase cachedb_mongodb cgrates \
        compression db_berkeley db_http db_mysql db_perlvdb db_postgres db_sqlite db_unixodbc dialplan emergency \
        event_rabbitmq h350 regex identity jabber json lua httpd mi_xmlrpc_ng mmgeoip perl pi_http rabbitmq proto_sctp proto_tls proto_wss \
        presence presence_dialoginfo presence_mwi presence_xml pua pua_bla pua_dialoginfo pua_mi pua_usrloc pua_xmpp \
        python rest_client rls siprec snmpstats tls_mgm xcap xcap_client xml xmpp ldap cpl_c" \
        basedir="$pkgdir" install
		
	chmod 750 "$pkgdir"/usr/etc/opensips/opensips.cfg
	install -d "$pkgdir"/var/run/opensips
	install -Dm755 ../$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
}



xmpp() { amove usr/lib64/opensips/modules/*xmpp*;}
xmpp() { amove usr/lib64/opensips/modules/*xmpp*;}
xmlrpc() { amove usr/lib64/opensips/modules/*xmlrpc*;}
wss() { amove usr/lib64/opensips/modules/proto_ws*;}
unixodbc() { amove usr/lib64/opensips/modules/db_unixodbc*;}
tlsmgm() { amove usr/lib64/opensips/modules/tls_mgm*;}
tls() { amove usr/lib64/opensips/modules/proto_tls*;}
sqlite() { amove usr/lib64/opensips/modules/*sqlite*;}
snmpstats() { amove usr/lib64/opensips/modules/*snmpstats*;}
sctp() { amove usr/lib64/opensips/modules/*sctp*;}
restclient() { amove usr/lib64/opensips/modules/*rest_client*;}
regex() { amove usr/lib64/opensips/modules/*regex*;}
redis() { amove usr/lib64/opensips/modules/*redis*;}
radius() { amove usr/lib64/opensips/modules/*radius*;}
presence() { 
	amove usr/lib64/opensips/modules/presence*;
	amove usr/lib64/opensips/modules/pua*;
	amove usr/lib64/opensips/modules/xcap*;
}
rabbit() { amove usr/lib64/opensips/modules/*rabbit*;}
b2bua() { amove usr/lib64/opensips/modules/b2b_*;}
berkeley() { amove usr/lib64/opensips/modules/db_berkeley*;}
compression() { amove usr/lib64/opensips/modules/compression*;}
dbhttp() { amove usr/lib64/opensips/modules/db_http*;}
dialplan() { amove usr/lib64/opensips/modules/dialplan*;}
emergency() { amove usr/lib64/opensips/modules/emergency*;}
geoip() { amove usr/lib64/opensips/modules/mmgeoip*;}
http() { amove usr/lib64/opensips/modules/httpd*;}
identity() { amove usr/lib64/opensips/modules/identity*;}
jabber() { amove usr/lib64/opensips/modules/jabber*;}
ldap() { amove usr/lib64/opensips/modules/ldap*;}
lua() { amove usr/lib64/opensips/modules/lua*;}
memcached() { amove usr/lib64/opensips/modules/cachedb_memcached*;}
mysql() { amove usr/lib64/opensips/modules/db_mysql*;}
postgres() { amove usr/lib64/opensips/modules/db_postgres*;}
perl() { amove usr/lib64/opensips/modules/*perl*;}















sha512sums="33cf476c2682bf57536b4ed81c22ed52ebbddb4a1a84908acb3bc1868c96743b92e0bc123b2b0964c1986b7b45c2966558414bdb5d680bef06c7560fb5eafb4e  opensips-2.4.8.tar.gz
e0c88e8fca1fdfe77db49c923f701edd358087a3c0e3bf1074bcf0dcf46d80c5f3011f3c059262f0b0e181d4cf94d0dc1b5e7ca487574dcd6789f3f161706bb1  opensips.initd
a8f0400a61f2c7438111bfff081e9457a986cfe51a29c53beaefea7857566b4038951483c8c54b0d01d29ea5a179402e11513f7ae3d23a2b5245d2eed814103e  10-resolve.patch
6cfa25aa59b45bc37efbb0b078cd0950ec219c1f16f13387e939f2effda3dcc36ada3b60149dd96a713fb0826871131785ac5dba303f26595897bfbb6dc4d45b  20-socket.patch
43f9d356e6a80d42739c69ac2ab8f9b0552a3f06483548d18696f18bd29709ef903cfba51bca618a6e416e693e7c4a33ab9c32240c6ae39cca926d37bee59baf  30-mathops.patch
8591a5457e9464e6a03d5cb890c14dc0a9d592678cf475827a21cdb7ea22ff1625a2f713a6a8c89b2e0ddca4166a97f254e6ff5fed95405b0006cdff6f087a2a  40-tls_mgm.patch
b845e3dd0fd82a761fa21c5a61b4a63ba6c75555361f788eef2497d566b83770f819c940377ae60dd07e5df8873a5574941329edcfab6ffa19aef5df9e5d8709  50-ldsession.patch"
